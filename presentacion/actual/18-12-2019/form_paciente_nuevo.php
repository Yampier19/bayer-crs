<?php
	include ('../logica/session.php');
	header("Content-Type: text/html;charset=utf-8");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Documento sin t&iacute;tulo</title>
<link type="text/css" rel="stylesheet" href="css/estilo_form_paciente.css" />
<script src="css/SpryAssets/SpryAccordion.js" type="text/javascript"></script>
<link href="css/SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />
<link href="css/estilo_form_paciente.css" type="text/css"/>
<!--<link type="text/css" rel="stylesheet" href="css/estilo_form_paciente.css" />-->

<script src="js/jquery.js"></script>
<script type="text/javascript" src="js/direccion.js"></script>
<script type="text/javascript" src="js/validar_campos_pacientes.js"></script>
<script type="text/javascript" src="js/validaciones.js"></script>
<script type="text/javascript" src="js/calcular_edad.js"></script>
<script type="text/javascript" src="js/validar_caracteres.js"></script>
<script type="text/javascript">
function trat_previo(sel) 
{
      if (sel.value=="Otro"){
		  
		   divC = document.getElementById("otro_tratamiento");
           divC.style.display = "";

         }
	  if (sel.value!="Otro"){
		  
		   divC = document.getElementById("otro_tratamiento");
           divC.style.display = "none";

         }	 
}

function mostrar_ciudades()
{
	var departamento=$('#departamento').val();	
	$("#ciudad").html('<img src="imgagenes/cargando.gif" />');
	$.ajax(
	{
		url:'../presentacion/ciudades.php',
		data:
		{
			dep: departamento,
		},
		type: 'post',
		beforeSend: function () 
		{
			$('#ciudad').attr('disabled');
			$("#ciudad").html("Procesando, espere por favor"+'<img src="img/cargando.gif" />');
		},
		success: function(data)
		{
			$('#ciudad').removeAttr("disabled");
			$('#ciudad').html(data);
		}
	}
	)
}
</script>
<script type="text/javascript" src="../logica/js/campos_form_nuevo.js" ></script>
<script type="text/javascript" src="../logica/js/validaciones_form_nuevo.js" ></script>
<style>
td
{
	padding: 6px;
	background-color:transparent;
}
.input__row{
  margin-top: 10px;  
}
/* Radio button */

/* Upload button */
.upload {
  display: none;
}
.uploader {
  border: 1px solid #12a9e3;
  width: 300px;
  position: relative;
  height: 30px;
  display: flex;
}
.uploader .input-value{
  width: 250px;
  padding: 5px;
  overflow: hidden;
  text-overflow: ellipsis;
  line-height: 25px;
  font-family: sans-serif;
  font-size: 16px;
}
.uploader label {
  cursor: pointer;
  margin: 0;
  width: 30px;
  height: 30px;
  position: absolute;
  right: 0;
  background: #17a8e391 url('https://www.interactius.com/wp-content/uploads/2017/09/folder.png') no-repeat center;
}
</style>
</head>
<?php
$string_intro = getenv("QUERY STRING"); 
parse_str($string_intro);
require('../datos/conex.php');
$DIAS_ANTES= date('Y-m-d', strtotime('-31 day')) ; // resta 31 d�a	
if($privilegios!=''&&$usuname_peru!='')
{
?>
<body class="body" style="width:80.9%;margin-left:12%;">
<form id="paciente_nuevo" name="paciente_nuevo" action="../logica/insertar_datos.php" method="post" enctype="multipart/form-data" class="letra">
<div id="Accordion1" class="Accordion" tabindex="0">
  <div class="AccordionPanel">
    <div class="AccordionPanelTab" style="padding:5px"><strong>GENERAL</strong></div>
    <div class="AccordionPanelContent">
<table width="100%" border="0">

  <tr>
  	<td width="20%">
	<span>Codigo de Usuario</span>
    </td>
    <td width="30%">
    <?php  
		$Seleccion = mysql_query("SELECT ID_PACIENTE FROM `bayer_pacientes` WHERE ID_PACIENTE != '' ORDER BY ID_PACIENTE DESC LIMIT 1",$conex);			
		while($fila=mysql_fetch_array($Seleccion))
			{
				
				$ID_PA = $fila['ID_PACIENTE'] + 1;
				function Zeros($numero, $largo) 
				{ 
				$resultado = $numero;
				while(strlen($resultado) < $largo) 
				{ 
				$resultado = "0".$resultado;  
				} 
				return $resultado;
				} 
				$ID_PACIENTE = Zeros($ID_PA, 5);  
			} 
  	?>
    <input type="radio" name="logro_comunicacion" id="logro_comunicacion" style=" width:20%;display:none" value="SI" checked="checked"/>
    <input name="codigo_usuario" type="text" id="codigo_usuario" max="10" readonly="readonly" value="<?php echo 'PAP'.$ID_PACIENTE; ?>"/> 
    <input name="codigo_usuario2" type="hidden" id="codigo_usuario2" max="10" readonly="readonly" value="<?php echo $ID_PACIENTE; ?>"/>            
    </td>
    <td width="20%">
	    <span>Estado del Paciente<span class="asterisco">*</span></span>
    </td>
    <td width="30%">
    <select type="text" name="estado_paciente" id="estado_paciente">
    	<option value="">Seleccione...</option>
        <option>Proceso</option>
<option>Activo</option>
<option>Abandono</option>

 	</select>
    </td>
    </tr>    
    <tr>
  	<td width="20%">
<span>Fecha de Activacion<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
    </td>
    <td width="30%">
  <input type="date" name="fecha_activacion" id="fecha_activacion" value="<?php echo date('Y-m-d'); ?>" readonly="readonly"/>
    </td>
    <td><span>Correo Electronico</span></td>
    <td><input type="text" name="correo" id="correo" /></td>
    </tr>
  <tr>
  <td>
  <span>Nombre<span class="asterisco">*</span></span>
  </td>  
  <td>
  <input type="text" name="nombre" id="nombre"/>
  </td>
  <td>
  <span>Apellidos<span class="asterisco">*</span></span>
  </td>
  <td>
  <input type="text" name="apellidos" id="apellidos"/> 
  </td>
  </tr>
     
  <tr>
  <td>
  <span>Identificacion<span class="asterisco">*</span></span>
  </td>  
  <td>
  <input type="text" name="identificacion" id="identificacion"/>
  </td>
  <td>
  <span>Telefono 1<span class="asterisco">*</span></span>
  </td>
  <td>
  <input type="text" name="telefono1" id="telefono1"/> 
  </td>
  </tr>
  
  <tr>
  <td>
  <span>Telefono 2</span>
  </td>  
  <td>
  <input type="text" name="telefono2" id="telefono2" />  
  </td>
  <td>
  <span>Telefono 3</span>
  </td>
  <td>
  <input type="text" name="telefono3" id="telefono3" /> 
  </td>
  </tr>	
  
  <tr>
    <td><span>Departamento<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
    <td><select type="text" name="departamento" id="departamento" onchange = "mostrar_ciudades()" style="text-transform:capitalize">
      <option value="">Seleccione...</option>
      <?php
		$Seleccion = mysql_query("SELECT NOMBRE_DEPARTAMENTO FROM `bayer_departamento` WHERE NOMBRE_DEPARTAMENTO != '' AND ID_PAIS_FK='1' ORDER BY NOMBRE_DEPARTAMENTO ASC",$conex);			
		while($fila=mysql_fetch_array($Seleccion))
			{
				$DEPARTAMENTO = $fila['NOMBRE_DEPARTAMENTO'];
				echo "<option>".$DEPARTAMENTO."</option>";
			} 
  	?>
    </select></td>
    <td><span>Ciudad<span class="asterisco">*</span></span></td>
    <td><select type="text" name="ciudad" id="ciudad">
      <option value="">Seleccione...</option>
    </select></td>
  </tr>	
  <tr>
    <td><span>Barrio<span class="asterisco">*</span></span></td>
    <td><input type="text" name="barrio" id="barrio" /></td>
      <td>&nbsp;</td>  
      <td>&nbsp;</td>
  </tr>
   <tr style="padding:3%;">
        <td style="width:10%;"><span>Direccion<span class="asterisco">*</span></span></td>
         <td bgcolor="#FFFFFF" colspan="3">
    <input type="text" name="DIRECCION" id="DIRECCION" readonly style="width:98.5%;"/>
    </td>
</tr>
<tr style="padding:3%;">
	<td><span>Via:</span></td>
    <td style="width:35%">
    <span>
    <select id="VIA" name="VIA" style="width:96%">
        <option value="">Seleccione...</option>
<option>ANILLO VIAL</option>
<option>AUTOPISTA</option>
 <option>AVENIDA</option>
<option>BOULEVAR</option>
<option>CALLE</option>
<option>CALLEJON</option>
<option>CARRERA</option>
<option>CIRCUNVALAR</option>
<option>CONDOMINIO</option>
<option>DIAGONAL</option>
<option>KILOMETRO</option>
<option>LOTE</option>
<option>SALIDA</option>
<option>SECTOR</option>
<option>TRANSVERSAL</option>
 <option>VEREDA</option>
<option>VIA</option>
    </select>
    </span></td>
    <td style="width:8%;"><span>Detalles Via:</span></td>
    <td width="177" bgcolor="#FFFFFF"><span>
    	<input name="detalle_via" id="detalle_via" type="text" maxlength="15" style="width:95%"/>
    </span>
    </td>
	</tr>
        <tr>
    <td width="96"><span>N&uacute;mero:</span></td>
    <td bgcolor="#FFFFFF">
    <span>
      <input name="numero" id="numero" type="text" maxlength="5" style="width:45%;"/>
      -
  <input name="numero2" id="numero2" type="text" maxlength="5" style="width:45%;"/>
    </span>
    </td>
    <td></td>
    <td bgcolor="#FFFFFF"></td>
    </tr>
	<tr style="padding:3%;">
    
    <td><span>Interior:</span></td>
    <td bgcolor="#FFFFFF"><span>
    <select id="interior" name="interior" style="width:96%">
    	<option value="">Seleccione...</option>
        <option>APARTAMENTO</option>
        <option>BARRIO</option>
<option>BLOQUE</option>
        <option>CASA</option>
        <option>CIUDADELA</option>
        <option>CONJUNTO</option>
        <option>CONJUNTO RESIDENCIAL</option>
        <option>EDIFICIO</option>
        <option>ENTRADA</option>
        <option>ETAPA</option>
        <option>INTERIOR</option>
        <option>MANZANA</option>
        <option>NORTE</option>
        <option>OFICINA</option>
        <option>OCCIDENTE</option>
        <option>ORIENTE</option>
        <option>PENTHOUSE</option>
        <option>PISO</option>
        <option>PORTERIA</option>
        <option>SOTANO</option>
        <option>SUR</option>
        <option>TORRE</option>
    </select>
    </span></td>
    <td><span>Detalles Interior:</span></td>
    <td bgcolor="#FFFFFF"><span>
    	<input name="detalle_int" id="detalle_int" type="text" maxlength="30" readonly style="width:95%"/>
    </span></td>
    
    </tr>
    <tr style="padding:3%;">
    <td><span>Interior:</span></td>
    <td bgcolor="#FFFFFF"><span>
    <select id="interior2" name="interior2" style="width:96%">
    	<option value="">Seleccione...</option>
        <option>APARTAMENTO</option>
        <option>BARRIO</option>
		<option>BLOQUE</option>
        <option>CASA</option>
        <option>CIUDADELA</option>
        <option>CONJUNTO</option>
        <option>CONJUNTO RESIDENCIAL</option>
        <option>EDIFICIO</option>
        <option>ENTRADA</option>
        <option>ETAPA</option>
        <option>INTERIOR</option>
        <option>MANZANA</option>
        <option>NORTE</option>
        <option>OFICINA</option>
        <option>OCCIDENTE</option>
        <option>ORIENTE</option>
        <option>PENTHOUSE</option>
        <option>PISO</option>
        <option>PORTERIA</option>
        <option>SOTANO</option>
        <option>SUR</option>
        <option>TORRE</option>
    </select>
    </span></td>
    <td><span>Detalles Interior:</span></td>
    <td bgcolor="#FFFFFF"><span>
    	<input name="detalle_int2" id="detalle_int2" type="text" maxlength="30" readonly style="width:95%"/>
    </span></td>
    
    </tr>
    <tr style="padding:3%;">
    <td><span>Interior:</span></td>
    <td bgcolor="#FFFFFF"><span>
    <select id="interior3" name="interior3" style="width:96%">
    	<option value="">Seleccione...</option>
        <option>APARTAMENTO</option>
        <option>BARRIO</option>
		<option>BLOQUE</option>
        <option>CASA</option>
        <option>CIUDADELA</option>
        <option>CONJUNTO</option>
        <option>CONJUNTO RESIDENCIAL</option>
        <option>EDIFICIO</option>
        <option>ENTRADA</option>
        <option>ETAPA</option>
        <option>INTERIOR</option>
        <option>MANZANA</option>
        <option>NORTE</option>
        <option>OFICINA</option>
        <option>OCCIDENTE</option>
        <option>ORIENTE</option>
        <option>PENTHOUSE</option>
        <option>PISO</option>
        <option>PORTERIA</option>
        <option>SOTANO</option>
        <option>SUR</option>
        <option>TORRE</option>
    </select>
    </span></td>
    <td><span>Detalles Interior:</span></td>
    <td bgcolor="#FFFFFF"><span>
    	<input name="detalle_int3" id="detalle_int3" type="text" maxlength="30" style="width:95%" readonly/>
    </span></td>
    
    </tr>
  
       
</table> 
    
    </div>
  </div>
  <div class="AccordionPanel">
    <div class="AccordionPanelTab"style="padding:5px"><strong>DETALLES</strong></div>
    <div class="AccordionPanelContent">

<table width="100%" border="0">
  <tr> 
    <td width="20%">
<span>Genero<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
    </td>  
    <td width="30%">
    <select type="text" name="genero" id="genero">
    	<option value="">Seleccione...</option>
        <option>Hombre</option>
        <option>Mujer</option>
 	</select>
    </td>  
    <td width="20%">
	<span>Fecha de Nacimiento<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
    </td>  
    <td width="30%">
	<input type="date" name="fecha_nacimiento" id="fecha_nacimiento"  max="<?php echo date('Y-m-d'); ?>"/>
    </td>  
    </tr>
    <tr>
    <td>
	    <span>Edad</span>
    </td>  
    <td>
	<input type="text" name="edad" id="edad" readonly="readonly"/>
    </td>  
    </tr>
    <tr>
    <td>
<!--<span class="titulos">ACUDIENTE</span>-->
<span>Acudiente&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
    </td>  
    <td>
	<input type="text" name="acudiente" id="acudiente"/>
    </td>  
    <td>
	<span>Telefono del Acudiente&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
    </td>  
    <td>
	<input type="text" name="telefono_acudiente" id="telefono_acudiente"/>
    </td>  
    </tr>
</table>

    </div>
  </div>
  <div class="AccordionPanel">
    <div class="AccordionPanelTab" style="padding:5px"><strong>INFORMACION DE TRATAMIENTO</strong></div>
    <div class="AccordionPanelContent">
<table width="100%" border="0">
	<?php
		$fecha_actual=date('Y-m-d');
		$fecha_rec_act = explode("-", $fecha_actual);
		$anio_act=$fecha_rec_act[0]; // a�o
		$mes_act=$fecha_rec_act[1]; // mes
		$dia_act=$fecha_rec_act[2]; // dia
		 $dato=((int)$mes_act);
		$ID=$fila['ID_PACIENTE'];
		$select_historial_pri=mysql_query("SELECT * FROM bayer_historial_reclamacion WHERE ID_PACIENTE_FK='$ID'",$conex);
	echo mysql_error($conex);
		$reg_hist=mysql_num_rows($select_historial_pri);
		if($reg_hist>0)
		{
			$select_historial=mysql_query("SELECT MES$dato as 'MES',RECLAMO$dato as 'RECLAMO',FECHA_RECLAMACION$dato as 'FECHA_RECLAMACION',MOTIVO_NO_RECLAMACION$dato as 'MOTIVO_NO_RECLAMACION' FROM bayer_historial_reclamacion WHERE ID_PACIENTE_FK='".$ID."' AND MES$dato='".$mes_act."'",$conex);
			echo mysql_error($conex);
			
		
			while($inf=mysql_fetch_array($select_historial))
			{
				$reclamo=$inf['RECLAMO'];
				$MES=$inf['MES'];
				$MOTIVO_NO_RECLAMACION=$inf['MOTIVO_NO_RECLAMACION'];
				$FECHA_RECLAMACION=$inf['FECHA_RECLAMACION'];
			}
		}
		else
		{
			$INSERT_HISTORIAL=mysql_query("INSERT INTO bayer_historial_reclamacion(ID_PACIENTE_FK) VALUES('".$fila['ID_PACIENTE']."')",$conex);
			echo mysql_error($conex);
		}
    ?>

	
    
  <tr> 
    <td width="20%">
    	<span>Producto<span class="asterisco">*</span></span>
    </td>
    <td style="width:30%;">
    	<input type="text" name="MEDICAMENTO" id="MEDICAMENTO" style="display:none"/>
        <select type="text" name="producto_tratamiento" id="producto_tratamiento">
    	<option>NEXAVAR 200MGX60C(12000MG)INST</option>
    	
        </select>
	</td>
    
    
</tr>

<tr>
  <!--<td><span>Status del Paciente</span></td>
  <td>
  <select type="text" name="status_paciente" id="status_paciente" disabled="disabled">
  </select>
  </td>-->
  <td width="20%"><span>Clasificacion Patologica<span class="asterisco">*</span></span></td>
  <td><span style="width:30%;">
    <select name="clasificacion_patologica" id="clasificacion_patologica">
	   <option>Seleccione...</option>
      <option>Carcinoma Hepatocelular</option>
<option>Cirrosis Hepatica</option>
<option>Infeccion Cronica por Hepatitis B/C</option>

    </select>
  </span></td> 
  <td><span>Consentimiento<span class="asterisco">*</span></span></td>
    <td><select type="text" name="consentimiento" id="consentimiento">
      <option value="">Seleccione...</option>
      <option>Fisico</option>
      <option>Verbal</option>
	  <option>SMS</option>
    </select></td>
  
    </tr>

<tr>
    
    
    </tr>
<tr>
    <td>
    	<span>Asegurador<span class="asterisco">*</span></span>
    </td>
    <td>
      <input name="asegurador" id="asegurador" type="text" style="width:78%;"/> 
    </td>
    <td><span>Regimen<span class="asterisco">*</span></span></td>
    <td><select type="text" name="regimen" id="regimen">
      <option value="">Seleccione...</option>
     <option>Contributivo</option>
<option>Especial</option>
<option>Subsidiado</option>

    </select></td>
    </tr>

<tr>
    <td><span>Ips que Atiende<span class="asterisco">*</span></span></td>
    <td><input type="text" name="ips_atiende" id="ips_atiende" /></td>
    
	<td>
    	<span>Medico<span class="asterisco">*</span></span>
    </td>
    <td>
    	<input type="text" name="medico" id="medico" maxlength="60" />
      <!--   <select type="text" name="medico" id="medico">
    		<option value="">Seleccione...</option>
	    </select>
      <select type="text" name="medico" id="medico">
        <option value="">Seleccione...</option>         
        <?php  
		$Seleccion = mysql_query("SELECT MEDICO FROM `bayer_listas` WHERE MEDICO != '' ORDER BY MEDICO ASC",$conex);			
		while($fila=mysql_fetch_array($Seleccion))
			{
				$MEDICO = $fila['MEDICO'];
				echo "<option>".$MEDICO."</option>";
			} 
  	?>
        </select>
      <span id="cual_medico" style="display:none;">Cual</span>
      <input type="text" name="medico_nuevo" id="medico_nuevo" style="display:none; width:84%"/>-->
    </td>
	
	
	
	
    </tr>
<tr>
	<td><span>Especialidad</span></td>
    <td><!--   <select type="text" name="especialidad" id="especialidad">
    		<option value="">Seleccione...</option>
 		</select> 
      <select type="text" name="especialidad" id="especialidad">
        <option value="">Seleccione...</option>
        <?php  
		$Seleccion = mysql_query("SELECT ESPECIALIDADES FROM `bayer_listas` WHERE ESPECIALIDADES != '' ORDER BY ESPECIALIDADES ASC",$conex);			
		while($fila=mysql_fetch_array($Seleccion))
			{
				$ESPECIALIDADES = $fila['ESPECIALIDADES'];
				echo "<option>".$ESPECIALIDADES."</option>";
			} 
  	?>
      </select>--> <input type="text" name="especialidad" id="especialidad" /></td>
	  
	  <td><span>Fecha de la Pr&oacute;xima Llamada<span class="asterisco">*</span></span></td>
    <td><input type="date" name="fecha_proxima_llamada" id="fecha_proxima_llamada"min="<?php echo date('Y-m-d'); ?>"/></td>
    <!---->
    </tr>




    <tr>
	<td><span>Examen</span></td>
		<td><select name="examen" id="examen">
		<option>Ecografia Hepato-biliar (EC)</option>
<option>Alfa-fetoproteina (AFP)</option>
<option>Tomografia axial computarizada (TAC), de cuatro fases dinamicas especializada en nodulo hepatico</option>
<option>Resonancia Magnetica (RM) sin y con medio de contraste</option>
<option>Creatinina</option>
<option>Nitrogeno ureico en sangre (BUN)</option>

		</select></td>
	</tr>
 
  <tr>
  	<td colspan="4">
    <div id="div_material_agregar" style="width:50%; margin:auto auto; display:none">
  		<iframe name="registro_productos_nuevo" style="width:100%; height:250px; border:1px solid #000;" scrolling="auto"></iframe>
    </div>
    </td>
  </tr>
</table>
<br />    
    </div>
  </div>

  <div class="AccordionPanel">
    <div class="AccordionPanelTab" style="padding:5px"><strong>NOTAS Y ADJUNTOS</strong></div>
    <div class="AccordionPanelContent">
<br />
<br />
<div style="width:91.4%;margin-left: 15px; ">
<textarea name="nota" id="nota" style="width:100%; height:100px" title="Escriba una Nota" placeholder="Escriba una Nota"></textarea>

</div>
<br />
<br />

<div style=" display: flex;  flex-wrap: wrap; margin-left: 15px; ">
<div style="flex: 0 0 50%;  max-width: 50%;">
 <span>Tipo de Adjunto<span class="asterisco"></span></span>
 <input type="radio" name="tipo_adjunto" id="tipo_adjunto" style=" width:5%; display:none" value="" checked="checked"/>
 <input type="radio" name="tipo_adjunto" id="tipo_adjunto" style=" width:5%;" value="Vaucher"/>Vaucher
   	<input type="radio" name="tipo_adjunto" id="tipo_adjunto" style=" width:5%;" value="CI"/>CI
<div class="input__row uploader">
  <div id="inputval" class="input-value"></div>
  <label for="archivo"></label>
 <input type="file" class="upload" name="archivo" id="archivo" class="aceptar">
</div>

<script>

 $('#archivo').on('change',function(){
   $('#inputval').text( $(this).val());
 });
</script></div>

</div>
<div style="flex: 0 0 33.333333%;
    max-width: 33.333333%;"></div>
<div style="flex: 0 0 33.333333%;
    max-width: 33.333333%;"></div>

<center>
<?php
	if($privilegios!=5)
	{
	?>
	<input id="registrar" name="registrar" type="submit" value="REGISTRAR" class="btn_registrar" onClick="return validar(paciente_nuevo,1)"/>
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<?PHP
	}
	?>
    </div>
  </div>
</div>
</form>
<script type="text/javascript">
var Accordion1 = new Spry.Widget.Accordion("Accordion1");
</script>
</body>
<?php
}
else
{
	?>
	<script type="text/javascript">
		window.onload = window.top.location.href = "../logica/cerrar_sesion2.php";
	</script>
	<?php
}
?>
</html>