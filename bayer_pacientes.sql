/*
SQLyog Community v8.71 
MySQL - 5.5.5-10.1.37-MariaDB : Database - bayer_pruebas_diagno
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bayer_pruebas_diagno` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci */;

USE `bayer_pruebas_diagno`;

/*Table structure for table `bayer_pacientes` */

DROP TABLE IF EXISTS `bayer_pacientes`;

CREATE TABLE `bayer_pacientes` (
  `ID_PACIENTE` bigint(5) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `ESTADO_PACIENTE` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `STATUS_PACIENTE` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  `FECHA_ACTIVACION_PACIENTE` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `FECHA_RETIRO_PACIENTE` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `MOTIVO_RETIRO_PACIENTE` varchar(30) COLLATE latin1_spanish_ci DEFAULT 'SIN INFORMACION',
  `OBSERVACION_MOTIVO_RETIRO_PACIENTE` varchar(400) COLLATE latin1_spanish_ci DEFAULT 'SIN INFORMACION',
  `IDENTIFICACION_PACIENTE` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `NOMBRE_PACIENTE` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `APELLIDO_PACIENTE` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `TELEFONO_PACIENTE` varchar(10) COLLATE latin1_spanish_ci DEFAULT '0',
  `TELEFONO2_PACIENTE` varchar(10) COLLATE latin1_spanish_ci DEFAULT '0',
  `TELEFONO3_PACIENTE` varchar(10) COLLATE latin1_spanish_ci DEFAULT '0',
  `CORREO_PACIENTE` varchar(150) COLLATE latin1_spanish_ci DEFAULT 'SIN INFORMACION',
  `DIRECCION_PACIENTE` varchar(400) COLLATE latin1_spanish_ci DEFAULT 'SIN INFORMACION',
  `BARRIO_PACIENTE` varchar(30) COLLATE latin1_spanish_ci DEFAULT 'SIN INFORMACION',
  `DEPARTAMENTO_PACIENTE` varchar(30) COLLATE latin1_spanish_ci DEFAULT 'SIN INFORMACION',
  `CIUDAD_PACIENTE` varchar(30) COLLATE latin1_spanish_ci DEFAULT 'SIN INFORMACION',
  `GENERO_PACIENTE` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `FECHA_NACIMINETO_PACIENTE` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `EDAD_PACIENTE` int(3) NOT NULL,
  `ACUDIENTE_PACIENTE` varchar(60) COLLATE latin1_spanish_ci NOT NULL,
  `TELEFONO_ACUDIENTE_PACIENTE` varchar(10) COLLATE latin1_spanish_ci DEFAULT '0',
  `ID_ULTIMA_GESTION` bigint(20) DEFAULT NULL,
  `USUARIO_CREACION` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`ID_PACIENTE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `bayer_pacientes` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
