<?php
	include ('../logica/session.php');
	header("Content-Type: text/html;charset=utf-8");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Documento sin t&iacute;tulo</title>
<link type="text/css" rel="stylesheet" href="css/estilo_form_paciente.css" />
<script src="css/SpryAssets/SpryAccordion.js" type="text/javascript"></script>
<link href="css/SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />
<link href="css/estilo_form_paciente.css" type="text/css"/>
<!--<link type="text/css" rel="stylesheet" href="css/estilo_form_paciente.css" />-->

<script src="js/jquery.js"></script>
<script type="text/javascript" src="js/direccion.js"></script>
<script type="text/javascript" src="js/validar_campos_pacientes.js"></script>
<script type="text/javascript" src="js/validaciones.js"></script>
<script type="text/javascript" src="js/calcular_edad.js"></script>
<script type="text/javascript" src="js/validar_caracteres.js"></script>
<script type="text/javascript">
function trat_previo(sel) 
{
      if (sel.value=="Otro"){
		  
		   divC = document.getElementById("otro_tratamiento");
           divC.style.display = "";

         }
	  if (sel.value!="Otro"){
		  
		   divC = document.getElementById("otro_tratamiento");
           divC.style.display = "none";

         }	 
}

function mostrar_ciudades()
{
	var departamento=$('#departamento').val();	
	$("#ciudad").html('<img src="imgagenes/cargando.gif" />');
	$.ajax(
	{
		url:'../presentacion/ciudades.php',
		data:
		{
			dep: departamento,
		},
		type: 'post',
		beforeSend: function () 
		{
			$('#ciudad').attr('disabled');
			$("#ciudad").html("Procesando, espere por favor"+'<img src="img/cargando.gif" />');
		},
		success: function(data)
		{
			$('#ciudad').removeAttr("disabled");
			$('#ciudad').html(data);
		}
	}
	)
}
</script>
<script type="text/javascript">
function mostrar_producto()
{
	var ID_PRODUCTO=$('#tipo_envio').val();	
	$.ajax(
	{
		url:'../presentacion/mostrar_nombre_producto.php',
		data:
		{
			ID_PRODUCTO: ID_PRODUCTO,
		},
		type: 'post',
		beforeSend: function () 
		{
			$('#div_agregar').css('visibility','hidden');
		},
		success: function(data)
		{
			$('#nombre_producto').html(data);
			
			var nom=$('#nombre_producto').val();
			//alert(nom);
			if(nom=='Kit de bienvenida'||nom=='')
			{
				$('#div_agregar').css('visibility','hidden');
			}
			else
			{
				$('#div_agregar').css('visibility','visible');
			}
		}
	}
	)
}
//AGREGAR PRODUCTO
function agregar_producto()
{
	var ID_PRODUCTO=$('#tipo_envio').val();
	var ID_PACIENTE=$('#codigo_usuario2').val();
	var NOMBRE_PRODUCTO=$('#nombre_producto').val();
	$.ajax(
	{
		url:'../presentacion/ingresar_productos_temporal.php',
		data:
		{
			ID_PRODUCTO: ID_PRODUCTO,
			ID_PACIENTE: ID_PACIENTE,
			NOMBRE_PRODUCTO:NOMBRE_PRODUCTO
		},
		type: 'post',
		beforeSend: function () 
		{
			$('#tabla_material_agregar').css('visibility','visible');
				$("#tabla_material_agregar").html("Procesando, espere por favor"+'<img src="imagenes/cargando.gif" />');
		},
		success: function(data)
		{
			
			//$('#div_tabla_productos').html('');
			
			$('#tabla_material_agregar').html(data);
		}
	}
	)
}
function materiales()
{
	var REFERENCIA=$('#producto_tratamiento').val();
	$.ajax(
	{
		url:'../presentacion/listado_producto_registrar.php',
		data:
		{
			REFERENCIA: REFERENCIA
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#tipo_envio").attr('disabled', 'disabled');
		},
		success: function(data)
		{
			$("#tipo_envio").removeAttr('disabled');
			$('#tipo_envio').html(data);
		}
	})
}
function status()
{
	var REFERENCIA=$('#producto_tratamiento').val();
	$.ajax(
	{
		url:'../presentacion/listado_producto_status.php',
		data:
		{
			REFERENCIA: REFERENCIA
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#status_paciente").attr('disabled', 'disabled');
		},
		success: function(data)
		{
			$("#status_paciente").removeAttr('disabled');
			$('#status_paciente').html(data);
		}
	})
}
function clasificacion()
{
	var REFERENCIA=$('#producto_tratamiento').val();
	$.ajax(
	{
		url:'../presentacion/listado_clasificacion_patologica.php',
		data:
		{
			REFERENCIA: REFERENCIA
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#clasificacion_patologica").attr('disabled', 'disabled');
		},
		success: function(data)
		{
			$("#clasificacion_patologica").removeAttr('disabled');
			$('#clasificacion_patologica').html(data);
		}
	})
}
/*function asegurador()
{
	var DEPT=$('#departamento').val();	
	$.ajax(
	{
		url:'../presentacion/listado_asegurador.php',
		data:
		{
			DEPT: DEPT
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#asegurador").attr('disabled', 'disabled');
			$('#operador_logistico').html('');
			$("#operador_logistico").attr('disabled', 'disabled');
		},
		success: function(data)
		{
			$("#asegurador").removeAttr('disabled');
			$('#asegurador').html(data);
		}
	})
}*/
function operador()
{
	var DEPT=$('#departamento').val();
	var asegurador=$('#asegurador').val();
	$.ajax(
	{
		url:'../presentacion/listado_operador_logistico.php',
		data:
		{
			DEPT: DEPT,
			asegurador: asegurador
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#operador_logistico").attr('disabled', 'disabled');
		},
		success: function(data)
		{
			$("#operador_logistico").removeAttr('disabled');
			$('#operador_logistico').html(data);
		}
	})
}
function mostrar_dosis()
{
	var reclamo=$('#reclamo').val();
	var MEDICAMENTO=$('#producto_tratamiento').val();
	if(reclamo=='SI'&& MEDICAMENTO=='BETAFERON CMBP X 15 VPFS (3750 MCG) MM')
	{
		$("#fecha_reclamacion").val($('#fecha_reclamacion').prop('defaultValue'));
		$("#consecutivo").val($('#consecutivo').prop('defaultValue'));
		$("#consecutivo_betaferon_span").css('display','block');
		$('#consecutivo_betaferon').css('display','block');
		
		$("#fecha_reclamacion_span").css('display','block');
		$('#fecha_reclamacion').css('display','block');
		
		$("#causa").css('display','none');
		$('#causa_no_reclamacion').css('display','none');
		$('#numero_cajas').removeAttr('disabled');
		$('#tipo_numero_cajas').removeAttr('disabled');
	}
	else
	{
		$("#consecutivo_betaferon_span").css('display','none');
		$('#consecutivo_betaferon').css('display','none');
	}

	var producto=$('#producto_tratamiento').val();
	//alert(producto);
	$.ajax(
	{
		url:'../presentacion/dosis.php',
		data:
		{
			producto: producto,
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#span_dosis").css('display','block');
			$("#span_dosis").html('<img src="imagenes/cargando.gif" />'+"  Procesando, espere por favor");
			$('#Dosis').attr('disabled');
			$("#Dosis").css('display','none');
		},
		success: function(data)
		{
			$("#Dosis option:eq(0)").attr("selected", "selected");
			$('#Dosis').val('');
			$('#Dosis3').val('');
			$("#span_dosis").css('display','none');
			$("#Dosis").css('display','block');
			$("#span_dosis").html("");		
			$('#Dosis').html(data);
			if(producto=='KOGENATE FS 2000 PLAN')
			{
				$('#Dosis3').css('display','block');
				$('#Dosis2').css('display','none');
				$('#Dosis').css('display','none');
			}
			if(producto=='Xofigo 1x6 ml CO')
			{
				$('#Dosis2').css('display','block');
				$('#Dosis3').css('display','none');
				$('#Dosis').css('display','none');
			}
			if(producto!='Xofigo 1x6 ml CO' && producto!='KOGENATE FS 2000 PLAN')
			{
				$('#Dosis').removeAttr("disabled");
				$('#Dosis2').css('display','none');
				$('#Dosis3').css('display','none');
				$('#Dosis').css('display','block');
			}
			//Dosis2
		}
	}
	)
}
</script>
<script>
/*DIRECCION*/
$(document).ready(function()
{
	function reclamo()
	{
		$("#causa_no_reclamacion option:eq(0)").attr("selected", "selected");
		$("#fecha_reclamacion").val('');
		
		var reclamo=$('#reclamo').val();
		var MEDICAMENTO=$('#producto_tratamiento').val();
		if(reclamo=='')
		{
			$("#causa").css('display','none');
			$('#causa_no_reclamacion').css('display','none');
			
			$("#fecha_reclamacion_span").css('display','none');
			$('#fecha_reclamacion').css('display','none');
			
			$("#consecutivo_betaferon_span").css('display','none');
			$('#consecutivo_betaferon').css('display','none');
			
			$('#numero_cajas option:eq(0)').attr('selected','selected');
			$('#tipo_numero_cajas option:eq(0)').attr('selected','selected');
			
			$('#numero_cajas').attr('disabled','disabled');
			$('#tipo_numero_cajas').attr('disabled','disabled');
		}
		if(reclamo=='NO')
		{
			$("#causa").css('display','block');
			$('#causa_no_reclamacion').css('display','block');
			
			$("#fecha_reclamacion_span").css('display','none');
			$('#fecha_reclamacion').css('display','none');
			
			$("#consecutivo_betaferon_span").css('display','none');
			$('#consecutivo_betaferon').css('display','none');
			
			$('#numero_cajas option:eq(0)').attr('selected','selected');
			$('#tipo_numero_cajas option:eq(0)').attr('selected','selected');
			$('#causa_no_reclamacion option:eq(1)').attr('selected','selected');
			
			$('#numero_cajas').attr('disabled','disabled');
			$('#tipo_numero_cajas').attr('disabled','disabled');
			
		}
		if(reclamo=='SI'&& MEDICAMENTO=='BETAFERON CMBP X 15 VPFS (3750 MCG) MM')
		{
			$("#fecha_reclamacion").val($('#fecha_reclamacion').prop('defaultValue'));
			$("#consecutivo").val($('#consecutivo').prop('defaultValue'));
			$("#consecutivo_betaferon_span").css('display','block');
			$('#consecutivo_betaferon').css('display','block');
			
			$("#fecha_reclamacion_span").css('display','block');
			$('#fecha_reclamacion').css('display','block');
			
			$("#causa").css('display','none');
			$('#causa_no_reclamacion').css('display','none');
			$('#numero_cajas').removeAttr('disabled');
			$('#tipo_numero_cajas').removeAttr('disabled');
		}
		else
		{
			if(reclamo=='SI')
			{
				$("#consecutivo_betaferon_span").css('display','none');
				$('#consecutivo_betaferon').css('display','none');
			
				$("#fecha_reclamacion_span").css('display','block');
				$('#fecha_reclamacion').css('display','block');
				
				$("#causa").css('display','none');
				$('#causa_no_reclamacion').css('display','none');
				$('#numero_cajas').removeAttr('disabled');
				$('#tipo_numero_cajas').removeAttr('disabled');
				$("#fecha_reclamacion").val($('#fecha_reclamacion').prop('defaultValue'));
			}
		}
	}
	reclamo();
	$("#reclamo").change(function()
	{
		reclamo();
	});
	$("#departamento").change(function()
	{
		//asegurador();
	});
	$("#asegurador").change(function()
	{
		operador();
	});
	$('#cambio').click(function()
	{
		$('#cambio_direccion').toggle();
		$('#DIRECCION').val('');		
		$("#VIA option:eq(0)").attr("selected", "selected");
		$("#interior option:eq(0)").attr("selected", "selected");
		$("#interior2 option:eq(0)").attr("selected", "selected");
		$("#interior3 option:eq(0)").attr("selected", "selected");
		$("#TERAPIA option:eq(0)").attr("selected", "selected");
		$('#detalle_via').val('');
		$('#detalle_int').val('');
		$('#detalle_int2').val('');
		$('#detalle_int3').val('');
		$('#numero').val('');
		$('#numero2').val('');

	});
	var via=$('#VIA').val();
	var dt_via=$('#detalle_via').val();
	$('#VIA').change(function()
	{
		dir();
	});
	
	$('#detalle_via').change(function()
	{
		dir();
	});
	$('#numero').change(function()
	{
		dir();
	});
	$('#numero2').change(function()
	{
		dir();
	});
	$('#interior').change(function()
	{
		dir();		
	});
	$('#detalle_int').change(function()
	{
		dir();
	});
	$('#interior2').change(function()
	{
		dir();		
	});
	$('#detalle_int2').change(function()
	{
		dir();
	});
	$('#interior3').change(function()
	{
		dir();		
	});
	$('#detalle_int3').change(function()
	{
		dir();
	});
	
	
});
/*FIN DIRECCION*/	
	
	
$(document).ready(function()
{
	
	var fecha=$('input[name=fecha_nacimiento]').val();
	if(fecha!='')
	{
		var edad=nacio(fecha);
		$("#edad").val(edad);
	}
	$("input[name=fecha_nacimiento]").change(function()
	{
		var fecha=$('input[name=fecha_nacimiento]').val();
		var edad=nacio(fecha);
		$("#edad").val(edad);
	});
	
	$("#medico").change(function()
	{
		$("#medico_nuevo").val('');
		
		var medico=$('#medico').val();
		if(medico=='Otro')
		{
			$('#medico_nuevo').css('display','inline-block');
			$('#cual_medico').css('display','inline-block');
		}
		if(medico!='Otro')
		{
			$('#medico_nuevo').css('display','none');
			$('#cual_medico').css('display','none');
		}

	});
	
	$("#producto_tratamiento").click(function mostrar_nebu()
	{	
		$("#nebulizaciones").val('');
		var producto_tratamiento=$('#producto_tratamiento').val();
		if(producto_tratamiento=='VENTAVIS 10 1SOL/2ML X30AMP(Conse) MM')
		{
			$('#span_nebulizaciones').css('display','inline-block');
			$('#div_nebulizaciones').css('display','inline-block');
		}
		if(producto_tratamiento!='VENTAVIS 10 1SOL/2ML X30AMP(Conse) MM')
		{
			$('#span_nebulizaciones').css('display','none');
			$('#div_nebulizaciones').css('display','none');
		}
	});
	
	$("#producto_tratamiento").click(function mostrar_tabletas()
	{	
		$("#numero_tabletas_diarias").val('');
		
		var reclamo=$('#reclamo').val();
		var producto_tratamiento=$('#producto_tratamiento').val();
		
		if(reclamo=='SI'&&producto_tratamiento=='NEXAVAR 200MGX60C(12000MG)INST'||producto_tratamiento=='ADEMPAS')
		{
			$('#span_tabletas_diarias').css('display','inline-block');
			$('#div_tabletas_diarias').css('display','inline-block');
		}
		if(producto_tratamiento!='NEXAVAR 200MGX60C(12000MG)INST'&&producto_tratamiento!='ADEMPAS')
		{
			$('#span_tabletas_diarias').css('display','none');
			$('#div_tabletas_diarias').css('display','none');
		}
		if(reclamo=='NO'||reclamo=='')
		{
			$('#span_tabletas_diarias').css('display','none');
			$('#div_tabletas_diarias').css('display','none');
		}
	});
	
	$("#reclamo").click(function mostrar_tabletas2()
	{	
		$("#numero_tabletas_diarias").val('');
		
		var reclamo=$('#reclamo').val();
		var producto_tratamiento=$('#producto_tratamiento').val();
		
		if(reclamo=='SI'&&producto_tratamiento=='NEXAVAR 200MGX60C(12000MG)INST'||producto_tratamiento=='ADEMPAS')
		{
			$('#span_tabletas_diarias').css('display','inline-block');
			$('#div_tabletas_diarias').css('display','inline-block');
		}
		if(producto_tratamiento!='NEXAVAR 200MGX60C(12000MG)INST'&&producto_tratamiento!='ADEMPAS')
		{
			$('#span_tabletas_diarias').css('display','none');
			$('#div_tabletas_diarias').css('display','none');
		}
		if(reclamo=='NO'||reclamo=='')
		{
			$('#span_tabletas_diarias').css('display','none');
			$('#div_tabletas_diarias').css('display','none');
		}
	});
	
	$("#producto_tratamiento").change(function()
	{
		$('nombre_producto').val('');
		mostrar_dosis();
	});
	$('#producto_tratamiento').change(function()
	{
		materiales();
		clasificacion();
		status();	
	});
	$("#tipo_envio").change(function()
	{
		mostrar_producto();
	});
	$("#agregar_nuevo").click(function()
	{
		$('#div_material_agregar').css('display','block');
		//$("#tipo_envio option:eq(0)").attr("selected", "selected");
		$('#div_agregar').css('visibility','hidden');
	});
});
</script>
<style>
td
{
	padding: 6px;
	background-color:transparent;
}
</style>
</head>
<?php
$string_intro = getenv("QUERY STRING"); 
parse_str($string_intro);
require('../datos/conex.php');
$DIAS_ANTES= date('Y-m-d', strtotime('-31 day')) ; // resta 31 d�a	
if($privilegios!=''&&$usuname_peru!='')
{
?>
<body class="body" style="width:80.9%;margin-left:12%;">
<form id="paciente_nuevo" name="paciente_nuevo" action="../logica/insertar_datos.php" method="post" enctype="multipart/form-data" class="letra">
<div id="Accordion1" class="Accordion" tabindex="0">
  <div class="AccordionPanel">
    <div class="AccordionPanelTab" style="padding:5px"><strong>GENERAL</strong></div>
    <div class="AccordionPanelContent">
<table width="100%" border="0">

  <tr>
  	<td width="20%">
	<span>Codigo de Usuario</span>
    </td>
    <td width="30%">
    <?php  
		$Seleccion = mysql_query("SELECT ID_PACIENTE FROM `bayer_pacientes` WHERE ID_PACIENTE != '' ORDER BY ID_PACIENTE DESC LIMIT 1",$conex);			
		while($fila=mysql_fetch_array($Seleccion))
			{
				
				$ID_PA = $fila['ID_PACIENTE'] + 1;
				function Zeros($numero, $largo) 
				{ 
				$resultado = $numero;
				while(strlen($resultado) < $largo) 
				{ 
				$resultado = "0".$resultado;  
				} 
				return $resultado;
				} 
				$ID_PACIENTE = Zeros($ID_PA, 5);  
			} 
  	?>
    <input type="radio" name="logro_comunicacion" id="logro_comunicacion" style=" width:20%;display:none" value="SI" checked="checked"/>
    <input name="codigo_usuario" type="text" id="codigo_usuario" max="10" readonly="readonly" value="<?php echo 'PAP'.$ID_PACIENTE; ?>"/> 
    <input name="codigo_usuario2" type="hidden" id="codigo_usuario2" max="10" readonly="readonly" value="<?php echo $ID_PACIENTE; ?>"/>            
    </td>
    <td width="20%">
	    <span>Estado del Paciente<span class="asterisco">*</span></span>
    </td>
    <td width="30%">
    <select type="text" name="estado_paciente" id="estado_paciente">
    	<option value="">Seleccione...</option>
        <option>Proceso</option>
<option>Activo</option>
<option>Abandono</option>

 	</select>
    </td>
    </tr>    
    <tr>
  	<td width="20%">
<span>Fecha de Activacion<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
    </td>
    <td width="30%">
  <input type="date" name="fecha_activacion" id="fecha_activacion" value="<?php echo date('Y-m-d'); ?>" readonly="readonly"/>
    </td>
    <td><span>Correo Electronico</span></td>
    <td><input type="text" name="correo" id="correo" /></td>
    </tr>
  <tr>
  <td>
  <span>Nombre<span class="asterisco">*</span></span>
  </td>  
  <td>
  <input type="text" name="nombre" id="nombre"/>
  </td>
  <td>
  <span>Apellidos<span class="asterisco">*</span></span>
  </td>
  <td>
  <input type="text" name="apellidos" id="apellidos"/> 
  </td>
  </tr>
     
  <tr>
  <td>
  <span>Identificacion<span class="asterisco">*</span></span>
  </td>  
  <td>
  <input type="text" name="identificacion" id="identificacion"/>
  </td>
  <td>
  <span>Telefono 1<span class="asterisco">*</span></span>
  </td>
  <td>
  <input type="text" name="telefono1" id="telefono1"/> 
  </td>
  </tr>
  
  <tr>
  <td>
  <span>Telefono 2</span>
  </td>  
  <td>
  <input type="text" name="telefono2" id="telefono2" />  
  </td>
  <td>
  <span>Telefono 3</span>
  </td>
  <td>
  <input type="text" name="telefono3" id="telefono3" /> 
  </td>
  </tr>	
  
  <tr>
    <td><span>Departamento<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
    <td><select type="text" name="departamento" id="departamento" onchange = "mostrar_ciudades()" style="text-transform:capitalize">
      <option value="">Seleccione...</option>
      <?php
		$Seleccion = mysql_query("SELECT NOMBRE_DEPARTAMENTO FROM `bayer_departamento` WHERE NOMBRE_DEPARTAMENTO != '' AND ID_PAIS_FK='1' ORDER BY NOMBRE_DEPARTAMENTO ASC",$conex);			
		while($fila=mysql_fetch_array($Seleccion))
			{
				$DEPARTAMENTO = $fila['NOMBRE_DEPARTAMENTO'];
				echo "<option>".$DEPARTAMENTO."</option>";
			} 
  	?>
    </select></td>
    <td><span>Ciudad<span class="asterisco">*</span></span></td>
    <td><select type="text" name="ciudad" id="ciudad">
      <option value="">Seleccione...</option>
    </select></td>
  </tr>	
  <tr>
    <td><span>Barrio<span class="asterisco">*</span></span></td>
    <td><input type="text" name="barrio" id="barrio" /></td>
      <td>&nbsp;</td>  
      <td>&nbsp;</td>
  </tr>
   <tr style="padding:3%;">
        <td style="width:10%;"><span>Direccion<span class="asterisco">*</span></span></td>
         <td bgcolor="#FFFFFF" colspan="3">
    <input type="text" name="DIRECCION" id="DIRECCION" readonly style="width:98.5%;"/>
    </td>
</tr>
<tr style="padding:3%;">
	<td><span>Via:</span></td>
    <td style="width:35%">
    <span>
    <select id="VIA" name="VIA" style="width:96%">
        <option value="">Seleccione...</option>
<option>ANILLO VIAL</option>
<option>AUTOPISTA</option>
 <option>AVENIDA</option>
<option>BOULEVAR</option>
<option>CALLE</option>
<option>CALLEJON</option>
<option>CARRERA</option>
<option>CIRCUNVALAR</option>
<option>CONDOMINIO</option>
<option>DIAGONAL</option>
<option>KILOMETRO</option>
<option>LOTE</option>
<option>SALIDA</option>
<option>SECTOR</option>
<option>TRANSVERSAL</option>
 <option>VEREDA</option>
<option>VIA</option>
    </select>
    </span></td>
    <td style="width:8%;"><span>Detalles Via:</span></td>
    <td width="177" bgcolor="#FFFFFF"><span>
    	<input name="detalle_via" id="detalle_via" type="text" maxlength="15" style="width:95%"/>
    </span>
    </td>
	</tr>
        <tr>
    <td width="96"><span>N&uacute;mero:</span></td>
    <td bgcolor="#FFFFFF">
    <span>
      <input name="numero" id="numero" type="text" maxlength="5" style="width:45%;"/>
      -
  <input name="numero2" id="numero2" type="text" maxlength="5" style="width:45%;"/>
    </span>
    </td>
    <td></td>
    <td bgcolor="#FFFFFF"></td>
    </tr>
	<tr style="padding:3%;">
    
    <td><span>Interior:</span></td>
    <td bgcolor="#FFFFFF"><span>
    <select id="interior" name="interior" style="width:96%">
    	<option value="">Seleccione...</option>
        <option>APARTAMENTO</option>
        <option>BARRIO</option>
<option>BLOQUE</option>
        <option>CASA</option>
        <option>CIUDADELA</option>
        <option>CONJUNTO</option>
        <option>CONJUNTO RESIDENCIAL</option>
        <option>EDIFICIO</option>
        <option>ENTRADA</option>
        <option>ETAPA</option>
        <option>INTERIOR</option>
        <option>MANZANA</option>
        <option>NORTE</option>
        <option>OFICINA</option>
        <option>OCCIDENTE</option>
        <option>ORIENTE</option>
        <option>PENTHOUSE</option>
        <option>PISO</option>
        <option>PORTERIA</option>
        <option>SOTANO</option>
        <option>SUR</option>
        <option>TORRE</option>
    </select>
    </span></td>
    <td><span>Detalles Interior:</span></td>
    <td bgcolor="#FFFFFF"><span>
    	<input name="detalle_int" id="detalle_int" type="text" maxlength="30" readonly style="width:95%"/>
    </span></td>
    
    </tr>
    <tr style="padding:3%;">
    <td><span>Interior:</span></td>
    <td bgcolor="#FFFFFF"><span>
    <select id="interior2" name="interior2" style="width:96%">
    	<option value="">Seleccione...</option>
        <option>APARTAMENTO</option>
        <option>BARRIO</option>
		<option>BLOQUE</option>
        <option>CASA</option>
        <option>CIUDADELA</option>
        <option>CONJUNTO</option>
        <option>CONJUNTO RESIDENCIAL</option>
        <option>EDIFICIO</option>
        <option>ENTRADA</option>
        <option>ETAPA</option>
        <option>INTERIOR</option>
        <option>MANZANA</option>
        <option>NORTE</option>
        <option>OFICINA</option>
        <option>OCCIDENTE</option>
        <option>ORIENTE</option>
        <option>PENTHOUSE</option>
        <option>PISO</option>
        <option>PORTERIA</option>
        <option>SOTANO</option>
        <option>SUR</option>
        <option>TORRE</option>
    </select>
    </span></td>
    <td><span>Detalles Interior:</span></td>
    <td bgcolor="#FFFFFF"><span>
    	<input name="detalle_int2" id="detalle_int2" type="text" maxlength="30" readonly style="width:95%"/>
    </span></td>
    
    </tr>
    <tr style="padding:3%;">
    <td><span>Interior:</span></td>
    <td bgcolor="#FFFFFF"><span>
    <select id="interior3" name="interior3" style="width:96%">
    	<option value="">Seleccione...</option>
        <option>APARTAMENTO</option>
        <option>BARRIO</option>
		<option>BLOQUE</option>
        <option>CASA</option>
        <option>CIUDADELA</option>
        <option>CONJUNTO</option>
        <option>CONJUNTO RESIDENCIAL</option>
        <option>EDIFICIO</option>
        <option>ENTRADA</option>
        <option>ETAPA</option>
        <option>INTERIOR</option>
        <option>MANZANA</option>
        <option>NORTE</option>
        <option>OFICINA</option>
        <option>OCCIDENTE</option>
        <option>ORIENTE</option>
        <option>PENTHOUSE</option>
        <option>PISO</option>
        <option>PORTERIA</option>
        <option>SOTANO</option>
        <option>SUR</option>
        <option>TORRE</option>
    </select>
    </span></td>
    <td><span>Detalles Interior:</span></td>
    <td bgcolor="#FFFFFF"><span>
    	<input name="detalle_int3" id="detalle_int3" type="text" maxlength="30" style="width:95%" readonly/>
    </span></td>
    
    </tr>
  
       
</table> 
    
    </div>
  </div>
  <div class="AccordionPanel">
    <div class="AccordionPanelTab"style="padding:5px"><strong>DETALLES</strong></div>
    <div class="AccordionPanelContent">

<table width="100%" border="0">
  <tr> 
    <td width="20%">
<span>Genero<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
    </td>  
    <td width="30%">
    <select type="text" name="genero" id="genero">
    	<option value="">Seleccione...</option>
        <option>Hombre</option>
        <option>Mujer</option>
 	</select>
    </td>  
    <td width="20%">
	<span>Fecha de Nacimiento<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
    </td>  
    <td width="30%">
	<input type="date" name="fecha_nacimiento" id="fecha_nacimiento"  max="<?php echo date('Y-m-d'); ?>"/>
    </td>  
    </tr>
    <tr>
    <td>
	    <span>Edad</span>
    </td>  
    <td>
	<input type="text" name="edad" id="edad" readonly="readonly"/>
    </td>  
    </tr>
    <tr>
    <td>
<!--<span class="titulos">ACUDIENTE</span>-->
<span>Representante Legal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
    </td>  
    <td>
	<input type="text" name="acudiente" id="acudiente"/>
    </td>  
    <td>
	<span>Telefono del Representante Legal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
    </td>  
    <td>
	<input type="text" name="telefono_acudiente" id="telefono_acudiente"/>
    </td>  
    </tr>
</table>

    </div>
  </div>
  <div class="AccordionPanel">
    <div class="AccordionPanelTab" style="padding:5px"><strong>INFORMACION DE TRATAMIENTO</strong></div>
    <div class="AccordionPanelContent">
<table width="100%" border="0">
	<?php
		$fecha_actual=date('Y-m-d');
		$fecha_rec_act = explode("-", $fecha_actual);
		$anio_act=$fecha_rec_act[0]; // a�o
		$mes_act=$fecha_rec_act[1]; // mes
		$dia_act=$fecha_rec_act[2]; // dia
		 $dato=((int)$mes_act);
		$ID=$fila['ID_PACIENTE'];
		$select_historial_pri=mysql_query("SELECT * FROM bayer_historial_reclamacion WHERE ID_PACIENTE_FK='$ID'",$conex);
	echo mysql_error($conex);
		$reg_hist=mysql_num_rows($select_historial_pri);
		if($reg_hist>0)
		{
			$select_historial=mysql_query("SELECT MES$dato as 'MES',RECLAMO$dato as 'RECLAMO',FECHA_RECLAMACION$dato as 'FECHA_RECLAMACION',MOTIVO_NO_RECLAMACION$dato as 'MOTIVO_NO_RECLAMACION' FROM bayer_historial_reclamacion WHERE ID_PACIENTE_FK='".$ID."' AND MES$dato='".$mes_act."'",$conex);
			echo mysql_error($conex);
			
		
			while($inf=mysql_fetch_array($select_historial))
			{
				$reclamo=$inf['RECLAMO'];
				$MES=$inf['MES'];
				$MOTIVO_NO_RECLAMACION=$inf['MOTIVO_NO_RECLAMACION'];
				$FECHA_RECLAMACION=$inf['FECHA_RECLAMACION'];
			}
		}
		else
		{
			$INSERT_HISTORIAL=mysql_query("INSERT INTO bayer_historial_reclamacion(ID_PACIENTE_FK) VALUES('".$fila['ID_PACIENTE']."')",$conex);
			echo mysql_error($conex);
		}
    ?>

	
    
  <tr> 
    <td width="20%">
    	<span>Producto<span class="asterisco">*</span></span>
    </td>
    <td style="width:30%;">
    	<input type="text" name="MEDICAMENTO" id="MEDICAMENTO" style="display:none"/>
        <select type="text" name="producto_tratamiento" id="producto_tratamiento">
    	<option>NEXAVAR 200MGX60C(12000MG)INST</option>
    	
        </select>
	</td>
    
    
</tr>

<tr>
  <!--<td><span>Status del Paciente</span></td>
  <td>
  <select type="text" name="status_paciente" id="status_paciente" disabled="disabled">
  </select>
  </td>-->
  <td width="20%"><span>Clasificacion Patologica<span class="asterisco">*</span></span></td>
  <td><span style="width:30%;">
    <select name="clasificacion_patologica" id="clasificacion_patologica">
	   <option>Seleccione...</option>
      <option>Carcinoma Hepatocelular</option>
<option>Cirrosis Hepatica</option>
<option>Infeccion Cronica por Hepatitis B/C</option>

    </select>
  </span></td> 
  <td><span>Consentimiento<span class="asterisco">*</span></span></td>
    <td><select type="text" name="consentimiento" id="consentimiento">
      <option value="">Seleccione...</option>
      <option>Fisico</option>
      <option>Verbal</option>
	  <option>SMS</option>
    </select></td>
  
    </tr>

<tr>
    
    
    </tr>
<tr>
    <td>
    	<span>Asegurador<span class="asterisco">*</span></span>
    </td>
    <td>
      <input name="asegurador" id="asegurador" type="text" style="width:78%;"/> 
    </td>
    <td><span>Regimen<span class="asterisco">*</span></span></td>
    <td><select type="text" name="regimen" id="regimen">
      <option value="">Seleccione...</option>
     <option>Contributivo</option>
<option>Especial</option>
<option>Subsidiado</option>

    </select></td>
    </tr>

<tr>
    <td><span>Ips que Atiende<span class="asterisco">*</span></span></td>
    <td><input type="text" name="ips_atiende" id="ips_atiende" /></td>
    
	<td>
    	<span>Medico<span class="asterisco">*</span></span>
    </td>
    <td>
    	<input type="text" name="medico" id="medico" maxlength="60" />
      <!--   <select type="text" name="medico" id="medico">
    		<option value="">Seleccione...</option>
	    </select>
      <select type="text" name="medico" id="medico">
        <option value="">Seleccione...</option>         
        <?php  
		$Seleccion = mysql_query("SELECT MEDICO FROM `bayer_listas` WHERE MEDICO != '' ORDER BY MEDICO ASC",$conex);			
		while($fila=mysql_fetch_array($Seleccion))
			{
				$MEDICO = $fila['MEDICO'];
				echo "<option>".$MEDICO."</option>";
			} 
  	?>
        </select>
      <span id="cual_medico" style="display:none;">Cual</span>
      <input type="text" name="medico_nuevo" id="medico_nuevo" style="display:none; width:84%"/>-->
    </td>
	
	
	
	
    </tr>
<tr>
	<td><span>Especialidad</span></td>
    <td><!--   <select type="text" name="especialidad" id="especialidad">
    		<option value="">Seleccione...</option>
 		</select> 
      <select type="text" name="especialidad" id="especialidad">
        <option value="">Seleccione...</option>
        <?php  
		$Seleccion = mysql_query("SELECT ESPECIALIDADES FROM `bayer_listas` WHERE ESPECIALIDADES != '' ORDER BY ESPECIALIDADES ASC",$conex);			
		while($fila=mysql_fetch_array($Seleccion))
			{
				$ESPECIALIDADES = $fila['ESPECIALIDADES'];
				echo "<option>".$ESPECIALIDADES."</option>";
			} 
  	?>
      </select>--> <input type="text" name="especialidad" id="especialidad" /></td>
	  
	  <td><span>Fecha de la Pr&oacute;xima Llamada<span class="asterisco">*</span></span></td>
    <td><input type="date" name="fecha_proxima_llamada" id="fecha_proxima_llamada"min="<?php echo date('Y-m-d'); ?>"/></td>
    <!---->
    </tr>




    <tr>
	<td><span>Examen</span></td>
		<td><select>
		<option>Ecografia Hepato-biliar (EC)</option>
<option>Alfa-fetoproteina (AFP)</option>
<option>Tomografia axial computarizada (TAC), de cuatro fases dinamicas especializada en nodulo hepatico</option>
<option>Resonancia Magnetica (RM) sin y con medio de contraste</option>
<option>Creatinina</option>
<option>Nitrogeno ureico en sangre (BUN)</option>

		</select></td>
	</tr>
    <!--
	<tr>
    <td>
        <span>Tipo de Envio</span>
        <br />
        <br />
    </td>
  	<td>
        <select name="tipo_envio" id="tipo_envio">
        <option value="">Seleccione...</option>
        <?php
        while($opcion=mysql_fetch_array($listado_envio))
		{
			?>
            <option value="<?php echo $opcion['ID_REFERENCIA'] ?>"><?php echo $opcion['MATERIAL'] ?></option>
            <?php
        }
        ?>
 		</select>
        <select name="nombre_producto" id="nombre_producto" style="display:none">
        </select>
        <br />
		<br />
    </td>
    <td>
        <div id="div_agregar" style="visibility:hidden">
            <input type="submit" name="agregar_nuevo" id="agregar_nuevo" formaction="form_productos_envio.php" formmethod="post" formtarget="registro_productos_nuevo" style="background-image:url(imagenes/agregar.png); background-repeat:no-repeat;  width:41px; height:38px; border:1px solid transparent; background-color:transparent" value=""/>
        </div>
    </td>
  </tr>-->
  <tr>
  	<td colspan="4">
    <div id="div_material_agregar" style="width:50%; margin:auto auto; display:none">
  		<iframe name="registro_productos_nuevo" style="width:100%; height:250px; border:1px solid #000;" scrolling="auto"></iframe>
    </div>
    </td>
  </tr>
</table>
<br />    
    </div>
  </div>
<!--  <div class="AccordionPanel">
    <div class="AccordionPanelTab">COMUNICACIONES</div>
    <div class="AccordionPanelContent">Contenido 5</div>
  </div>-->
  <div class="AccordionPanel">
    <div class="AccordionPanelTab" style="padding:5px"><strong>NOTAS Y ADJUNTOS</strong></div>
    <div class="AccordionPanelContent">
<br />
<br />
<div style="width:91.4%;">
<textarea name="nota" id="nota" style="width:100%; height:100px" title="Escriba una Nota" placeholder="Escriba una Nota" onKeyDown="return filtro(1)"></textarea>
</div>
<br />
<br />
<div style="width:91.4%;">
<input type="file" name="archivo" id="archivo" class="aceptar"></input>
</div>
<center>
<?php
	if($privilegios!=5)
	{
	?>
	<input id="registrar" name="registrar" type="submit" value="REGISTRAR" class="btn_registrar" onClick="return validar(paciente_nuevo,1)"/>
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<?PHP
	}
	?>
    </div>
  </div>
</div>
</form>
<script type="text/javascript">
var Accordion1 = new Spry.Widget.Accordion("Accordion1");
</script>
</body>
<?php
}
else
{
	?>
	<script type="text/javascript">
		window.onload = window.top.location.href = "../logica/cerrar_sesion2.php";
	</script>
	<?php
}
?>
</html>