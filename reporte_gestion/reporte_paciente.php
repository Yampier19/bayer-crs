<?php 
$conex=mysqli_connect('localhost','root','','bayer_crs')or die ("no se pudo");
mysqli_select_db($conex,'bayer_crs') or die ("no se puede conectar a la database");   
/*
$conex=mysqli_connect('app-peoplemarketing.com','apppeopl','ser1_pE0p1E*2018','apppeopl_bayer')or die ("no se pudo");
mysqli_select_db($conex,'apppeopl_bayer') or die ("no se puede conectar a la database"); */

?>
<!DOCTYPE html>
<html lang="es">
    <!-- META-->
    <title>Reportes </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- AJAX-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- BOOOTSTRAP -->
    <link href="../presentacion/css/bootstrap_reporte.css" rel="stylesheet" />
    <!-- JQUERY PAGINIADO-->
    <script type="text/javascript" src="../presentacion/js/reporte_gestion.js"></script>
    <!-- EXPORTABLE-->
    <link href="../presentacion/css/style_reporte_gestion.css" rel="stylesheet" type="text/css">
    
    <!--ESTILO DEL PAGINIADO-->
    <link rel="stylesheet" type="text/css" href="../presentacion/css/jquery_reporte_gestion.css"/>
    <link href="../presentacion/css/estilo_menu_reporte_gestion.css" rel="stylesheet" type="text/css">
    <!-- FUNCION DEL PAGINIADO -->
    <script type="text/javascript">
        $(document).ready(function () {

            $('#usertable').DataTable();


        });
    </script>
    <script type="text/javascript">

        $(document).ready(function () {

            $('#SELECTOR').change(function ()
            {

                var x = $('#SELECTOR').val();
                if (x == '<' || x == '>' || x == '') {

                    $("#division1").css('display', 'inline');
                    $("#division2").css('display', 'none');
                }
                if (x == 'total') {

                    $("#division1").css('display', 'none');
                    $("#division2").css('display', 'none');
                }
            });
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#Busqueda').click(function () {

                $("#super_busqueda").css('display', 'inline-block');
                $("#ocultar_boton").css('display', 'none');

            });
            $('#Cancelar').click(function () {

                $("#super_busqueda").css('display', 'none');

            });
        });
    </script>
 
</head>
<body>
 <div class="body">
    <div class="container" style="margin-top:-35px;">
        <div class="row">
            <div class="col-sm-12">
                <h2>Reporte Pacientes</h2>
                <div class="boton_filtrar">
                    <div class="classs1" id="ocultar_boton">
                        <input type="button" value="Filtrar" class="Busquedaa" id="Busqueda"></input>
                    </div>
                    <div class="classs2" id="super_busqueda" name="super_busqueda" style="display:none ">
                        <form method="post" action="#">
                            <h3 class="ppp">Consulta Avanzada <span class="sub_fecha">  <br>  -Fecha Activacion Paciente  </span>:</h3>
                            <div class="selecionnn">
                                <SELECT ID= "SELECTOR" NAME="SELECTOR" >
                                    <OPTION VALUE="total">Total</OPTION>
                                    <OPTION VALUE=">">Mayor que</OPTION>
                                    <OPTION VALUE="<">Menor que</OPTION>
                                    <OPTION VALUE="">Exactamente igual</OPTION>


                                </SELECT>
                            </div>

                            <span style="display: none" class="spann1" id="division1" name="division1"> 
                                <input type="date" name="input_n1" id="input_n1" placeholder="AAAA-MM-DD">
                            </span>
                            <span class="bttp">
                                <button class="btn_buscar" name="enviar" type="submit" id="enviar">Buscar</button>
                                <button class="btn_buscar" name="Cancelar" type="submit" id="Cancelar">Cancelar</button>
                            </span>
                            <span class="spann2" style="display: none;" id="division2" name="division2">
                                <input type="text" name="input_n2" id="input_n2" placeholder="AAAA-MM-DD">
                            </span>


                            <div class="col-sm-4"></div>
                    </div>
                    </form>

                </div>
                <div class="table-responsive">
                    <div class="tabla1">
                        <table id="usertable" class="display">
                            <thead>
                                <tr>
                                    <td>ID PACIENTE</td>                      
                                    <td>ESTADO PACIENTE</td>
                                    <td>FECHA ACTIVACIONPACIENTE</td>
                                    <td>FECHA RETIRO PACIENTE</td>                            
                                    <td>MOTIVO RETIRO PACIENTE</td>
                                    <td>OBSERVACION MOTIVO RETIRO PACIENTE</td>
                                    <td>IDENTIFICACION PACIENTE</td>
                                    <td>NOMBRE PACIENTE</td>                    
                                    <td>APELLIDO PACIENTE</td>
                                    <td>TELEFONO PACIENTE</td>
                                    <td>TELEFONO2 PACIENTE</td>                         
                                    <td>TELEFONO3 PACIENTE</td>
                                    <td>CORREO PACIENTE</td>
                                    <td>DIRECCION PACIENTE</td>
                                    <td>BARRIO PACIENTE</td>
                                    <td>DEPARTAMENTO PACIENTE</td>
                                    <td>CIUDAD PACIENTE</td>
                                    <td>GENERO PACIENTE</td>
                                    <td>FECHA NACIMINETO PACIENTE</td>
                                    <td>EDAD PACIENTE</td>
                                    <td>ACUDIENTE PACIENTE</td>
                                    <td>TELEFONO ACUDIENTE PACIENTE</td>
                                   
                                   
                                    <td>ID ULTIMA GESTION</td>
                                    <td>USUARIO CREACION</td>
                                    <td>ID TRATAMIENTO</td>
                                    <td>PRODUCTO TRATAMIENTO</td>
                                    <td>NOMBRE REFERENCIA</td>
                                    <td>CLASIFICACION PATOLOGICA TRATAMIENTO</td>
                                    <td>TRATAMIENTO PREVIO</td>
                                    <td>CONSENTIMIENTO TRATAMIENTO</td>
                                    <td>FECHA INICIO TERAPIA TRATAMIENTO</td>
                                    <td>REGIMEN TRATAMIENTO</td>
                                    <td>ASEGURADOR TRATAMIENTO</td>
                                    <td>OPERADOR LOGISTICO TRATAMIENTO</td>
                                  
                                    <td>FECHA ULTIMA RECLAMACION TRATAMIENTO</td>
                                    <td>OTROS OPERADORES TRATAMIENTO</td>
                                    <td>MEDIOS ADQUISICION TRATAMIENTO</td>
                                    <td>IPS ATIENDE TRATAMIENTO</td>
                                    <td>MEDICO TRATAMIENTO</td>
                                    <td>ESPECIALIDAD TRATAMIENTO</td>
                                    <td>PARAMEDICO TRATAMIENTO</td>
                                    <td>ZONA ATENCION PARAMEDICO TRATAMIENTO</td>
                                    <td>CIUDAD BASE PARAMEDICO TRATAMIENTO</td>
                                    <td>NOTAS ADJUNTOS TRATAMIENTO</td>


                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                if(TRUE){



                                if (TRUE){

                                $DOCUMENTO_FIL = $_POST['input_n1'];
                                $OPERADOR = $_POST['SELECTOR'];



                                $sqlpp = mysqli_query($conex, 'SELECT    A.ID_PACIENTE AS ID_PACIENTE,
                                                                            A.ESTADO_PACIENTE AS ESTADO_PACIENTE,
                                                                            A.FECHA_ACTIVACION_PACIENTE AS FECHA_ACTIVACION_PACIENTE,
                                                                            A.FECHA_RETIRO_PACIENTE AS FECHA_RETIRO_PACIENTE,
                                                                            A.IDENTIFICACION_PACIENTE AS IDENTIFICACION_PACIENTE,
                                                                            A.NOMBRE_PACIENTE AS NOMBRE_PACIENTE,
                                                                            A.APELLIDO_PACIENTE AS APELLIDO_PACIENTE,
                                                                            A.TELEFONO_PACIENTE AS TELEFONO_PACIENTE,
                                                                            A.TELEFONO2_PACIENTE AS TELEFONO2_PACIENTE,
                                                                            A.TELEFONO3_PACIENTE AS TELEFONO3_PACIENTE,
                                                                            A.CORREO_PACIENTE AS CORREO_PACIENTE,
                                                                            A.DIRECCION_PACIENTE AS DIRECCION_PACIENTE,
                                                                            A.BARRIO_PACIENTE AS BARRIO_PACIENTE,
                                                                            A.DEPARTAMENTO_PACIENTE AS DEPARTAMENTO_PACIENTE,
                                                                            A.CIUDAD_PACIENTE AS CIUDAD_PACIENTE,
                                                                            A.GENERO_PACIENTE AS GENERO_PACIENTE,
                                                                            A.FECHA_NACIMINETO_PACIENTE AS FECHA_NACIMINETO_PACIENTE,
                                                                            A.EDAD_PACIENTE AS EDAD_PACIENTE,
                                                                            A.ACUDIENTE_PACIENTE AS ACUDIENTE_PACIENTE,
                                                                            A.TELEFONO_ACUDIENTE_PACIENTE AS TELEFONO_ACUDIENTE_PACIENTE,
                                                                            CODIGO_XOFIGO AS CODIGO_XOFIGO,
                                                                            A.STATUS_PACIENTE AS STATUS_PACIENTE,
                                                                            A.ID_ULTIMA_GESTION AS ID_ULTIMA_GESTION,
                                                                            A.USUARIO_CREACION AS USUARIO_CREACION,
                                                                            B.ID_TRATAMIENTO AS ID_TRATAMIENTO,
                                                                            B.CLASIFICACION_PATOLOGICA_TRATAMIENTO AS CLASIFICACION_PATOLOGICA_TRATAMIENTO,
                                                                            B.CONSENTIMIENTO_TRATAMIENTO AS CONSENTIMIENTO_TRATAMIENTO,
                                                                            B.ASEGURADOR_TRATAMIENTO AS ASEGURADOR_TRATAMIENTO,
                                                                            B.IPS_ATIENDE_TRATAMIENTO AS IPS_ATIENDE_TRATAMIENTO,
                                                                            B.MEDICO_TRATAMIENTO AS MEDICO_TRATAMIENTO,
                                                                            B.ESPECIALIDAD_TRATAMIENTO AS ESPECIALIDAD_TRATAMIENTO,
                                                                            B.EXAMEN AS EXAMEN,
                                                                            CENTRO_DE_DIAGNÓSTICO AS CENTRO_DE_DIAGNÓSTICO,
                                                                            B.NOTAS_ADJUNTOS_TRATAMIENTO AS NOTAS_ADJUNTOS_TRATAMIENTO,
                                                                            VAUCHER AS VÁUCHER,
                                                                            CI AS CI
                                FROM bayer_pacientes AS A
                                LEFT JOIN bayer_tratamiento AS B ON A.ID_PACIENTE =B.ID_PACIENTE_FK');

                                while ($datos =(mysqli_fetch_array($sqlpp))) {?>                          
                                <tr>    
                                    <td><?php echo $datos['ID_PACIENTE']; ?></td>                      
                                    <td><?php echo $datos['ESTADO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['FECHA_ACTIVACION_PACIENTE']; ?></td>
                                    <td><?php echo $datos['FECHA_RETIRO_PACIENTE']; ?></td>                         
                                    <td><?php echo $datos['IDENTIFICACION_PACIENTE']; ?></td>
                                    <td><?php echo $datos['NOMBRE_PACIENTE']; ?></td>                    
                                    <td><?php echo $datos['APELLIDO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['TELEFONO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['TELEFONO2_PACIENTE']; ?></td>                         
                                    <td><?php echo $datos['TELEFONO3_PACIENTE']; ?></td>
                                    <td><?php echo $datos['CORREO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['DIRECCION_PACIENTE']; ?></td>
                                    <td><?php echo $datos['BARRIO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['DEPARTAMENTO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['CIUDAD_PACIENTE']; ?></td>
                                    <td><?php echo $datos['GENERO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['FECHA_NACIMINETO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['EDAD_PACIENTE']; ?></td>
                                    <td><?php echo $datos['ACUDIENTE_PACIENTE']; ?></td>
                                    <td><?php echo $datos['TELEFONO_ACUDIENTE_PACIENTE']; ?></td>
                                    <td><?php echo $datod['CODIGO_XOFIGO']?></td>
                                    <td><?php echo $datos['STATUS_PACIENTE'];?></td>
                                    <td><?php echo $datos['ID_ULTIMA_GESTION']; ?></td>
                                    <td><?php echo $datos['USUARIO_CREACION']; ?></td>
                                    <td><?php echo $datos['ID_TRATAMIENTO']; ?></td>
                                    <td><p><?php echo $datos['CLASIFICACION_PATOLOGICA_TRATAMIENTO']; ?></p></td>
                                    <td><p><?php echo $datos['CONSENTIMIENTO_TRATAMIENTO']; ?></p></td>
                                    <td><p><?php echo $datos['ASEGURADOR_TRATAMIENTO']; ?></p></td>
                                    <td><p><?php echo $datos['IPS_ATIENDE_TRATAMIENTO']; ?></p></td>
                                    <td><p><?php echo $datos['MEDICO_TRATAMIENTO']; ?></p></td>
                                    <td><p><?php echo $datos['ESPECIALIDAD_TRATAMIENTO']; ?></p></td>
                                    <td><p><?php echo $datos['EXAMEN'] ?></p></td> 
                                    <td><p><?php echo $datos['CENTRO_DE_DIAGNÓSTICO']?></p></td>
                                    <td><p><?php echo $datos['NOTAS_ADJUNTOS_TRATAMIENTO']; ?></p></td>
                                    <td><p><?php echo $datos['VÁUCHER']?></p></P></td>
                                    <td><p><?php echo $datos['CI']?></p></td>
                                </tr>
                                <?php }
                                } else if($_POST['SELECTOR'] == 'total'){
                        $sqlpp = mysqli_query($conex, 'SELECT    ID_PACIENTE,
                                                                    ESTADO_PACIENTE,
                                                                    FECHA_ACTIVACION_PACIENTE,
                                                                    FECHA_RETIRO_PACIENTE,
                                                                    IDENTIFICACION_PACIENTE,
                                                                    NOMBRE_PACIENTE,
                                                                    APELLIDO_PACIENTE,
                                                                    TELEFONO_PACIENTE,
                                                                    TELEFONO2_PACIENTE,
                                                                    TELEFONO3_PACIENTE,
                                                                    CORREO_PACIENTE,
                                                                    DIRECCION_PACIENTE,
                                                                    BARRIO_PACIENTE,
                                                                    DEPARTAMENTO_PACIENTE,
                                                                    CIUDAD_PACIENTE,
                                                                    GENERO_PACIENTE,
                                                                    FECHA_NACIMINETO_PACIENTE,
                                                                    EDAD_PACIENTE,
                                                                    ACUDIENTE_PACIENTE,
                                                                    TELEFONO_ACUDIENTE_PACIENTE,
                                                                    CODIGO_XOFIGO,
                                                                    STATUS_PACIENTE,
                                                                    ID_ULTIMA_GESTION,
                                                                    USUARIO_CREACION,
                                                                    ID_TRATAMIENTO,
                                                                    CLASIFICACION_PATOLOGICA_TRATAMIENTO,
                                                                    CONSENTIMIENTO_TRATAMIENTO,
                                                                    ASEGURADOR_TRATAMIENTO,
                                                                    IPS_ATIENDE_TRATAMIENTO,
                                                                    MEDICO_TRATAMIENTO,
                                                                    ESPECIALIDAD_TRATAMIENTO,
                                                                    EXAMEN,
                                                                    CENTRO_DE_DIAGNÓSTICO,
                                                                    NOTAS_ADJUNTOS_TRATAMIENTO,
                                                                    VÁUCHER,
                                                                    CI desc ');
                                }  while ($datos =(mysqli_fetch_array($sqlpp)))?> {
                                <tr>   
                                    <td><?php echo $datos['ID_PACIENTE']; ?></td>
                                    <td><?php echo $datos['ESTADO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['FECHA_ACTIVACION_PACIENTE']; ?></td>
                                    <td><?php echo $datos['FECHA_RETIRO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['IDENTIFICACION_PACIENTE']; ?></td>
                                    <td><?php echo $datos['NOMBRE_PACIENTE']; ?></td> 
                                    <td><?php echo $datos['APELLIDO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['TELEFONO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['TELEFONO2_PACIENTE']; ?></td> 
                                    <td><?php echo $datos['TELEFONO3_PACIENTE']; ?></td>
                                    <td><?php echo $datos['CORREO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['DIRECCION_PACIENTE']; ?></td>
                                    <td><?php echo $datos['BARRIO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['DEPARTAMENTO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['CIUDAD_PACIENTE']; ?></td>
                                    <td><?php echo $datos['GENERO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['FECHA_NACIMINETO_PACIENTE']; ?></td>
                                    <td><?php echo $datos['EDAD_PACIENTE']; ?></td>
                                    <td><?php echo $datos['ACUDIENTE_PACIENTE']; ?></td>
                                    <td><?php echo $datos['TELEFONO_ACUDIENTE_PACIENTE']; ?></td>
                                    <td><?php echo $datos['CODIGO_XOFIGO']; ?></td>
                                    <td><?php echo $datos['STATUS_PACIENTE']; ?></td>
                                    <td><?php echo $datos['ID_ULTIMA_GESTION']; ?></td>
                                    <td><?php echo $datos['USUARIO_CREACION']; ?></td>
                                    <td><?php echo $datos['ID_TRATAMIENTO']; ?></td>
                                    <td><p><?php echo $datos['CLASIFICACION_PATOLOGICA_TRATAMIENTO']; ?></p></td>
                                    <td><p><?php echo $datos['CONSENTIMIENTO_TRATAMIENTO']; ?></p></td>
                                    <td><p><?php echo $datos['ASEGURADOR_TRATAMIENTO']; ?></p></td>
                                    <td><p><?php echo $datos['IPS_ATIENDE_TRATAMIENTO']; ?></p></td>
                                    <td><p><?php echo $datos['MEDICO_TRATAMIENTO']; ?></p></td>
                                    <td><p><?php echo $datos['ESPECIALIDAD_TRATAMIENTO']; ?></p></td>
                                    <td><p><?php echo $datos['EXAMEN']; ?></p></td>
                                    <td><p><?php echo $datos['CENTRO_DE_DIAGNÓSTICO']; ?></p></td>
                                    <td><p><?php echo $datos['NOTAS_ADJUNTOS_TRATAMIENTO']; ?></p></td>
                                    <td><p><?php echo $datos['VÁUCHER']; ?></p></td>
                                    <td><p><?php echo $datos['CI']; ?></p></td>
                                
                            </tr> 
                                }
                                 </tbody></table>
                    </div>      
                </div>
            </div>
        </div>
    </div>
	</div>
    <?php } ?>
    <footer class="footer fixed-bottom">
        <div class="container">
            <span class="text-muted"></span>
        </div>
    </footer>
</body>
</html>
<!-- JQUERY EXPORTABLE--->
<script src="../presentacion/js/jquery-1.12.4.min.js"></script>
<!-- Llamar a los complementos javascript EXPORTABLE-->
<script src="../presentacion/js/FileSaver.min.js"></script>
<script src="../presentacion/js/Blob.min.js"></script>
<script src="../presentacion/js/xls.core.min.js"></script>
<script src="../presentacion/js/tableexport.js"></script>
<!-- JS DE PAGINIADO-->
<script type="text/javascript" src="../presentacion/js/jquery.dataTables.js"></script>
<!-- FUNCION DE LA EXPORTACION-->
<script>
        $("table").tableExport({
            formats: ["xlsx", "txt", "csv"], //Tipo de archivos a exportar ("xlsx","txt", "csv", "xls")
            position: 'button', // Posicion que se muestran los botones puedes ser: (top, bottom)
            bootstrap: false, //Usar lo estilos de css de bootstrap para los botones (true, false)
            fileName: "pad_reporte_gestiones", //Nombre del archivo 
        });
</script>


